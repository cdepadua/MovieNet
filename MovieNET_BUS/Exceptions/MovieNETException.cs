﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieNET_BUS.Exceptions
{
    public class MovieNETException : Exception
    {
        public MovieNETException()
        {
        }

        public MovieNETException(string message) : base(message)
        {
        }
    }
}
