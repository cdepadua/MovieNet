﻿using MovieNET_BUS.Services;
using MovieNET_BUS.Tools;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieNET_BUS
{
    public class MovieNETBusiness
    {
        private static MovieNETBusiness _movieNETBusinessInstance;

        /** User Service. */
        private UserService _userService;
        private FilmService _filmService;

        /** Constructeur. */
        private MovieNETBusiness()
        {
            _userService = UserService.UserServiceInstance;
            _filmService = FilmService.FilmServiceInstance;
        }

        /** Getter instance sngleton. */
        public static MovieNETBusiness MovieNETBusinessInstance
        {
            get
            {
                if (_movieNETBusinessInstance == null)
                {
                    _movieNETBusinessInstance = new MovieNETBusiness();
                }
                return _movieNETBusinessInstance;
            }
        }

        /** Encrypt String.*/
        public String Encrypt(String str)
        {
            return StringCipher.Encrypt(str, Constants.PASSPHRASE);
        }

        /** User Registration. */
        public void userRegistration (User user)
        {
            _userService.userRegistration(user);
        }
        /** User Login. */
        public User userLogin (String email, String password)
        {
           return _userService.userLogin(email, password);
        }
        /** Get Category List. */
        public ObservableCollection<Category> GetCategoryList()
        {
            return _filmService.GetCategories();
        }
        /** Add Film. */
        public void AddFilm(String title, Category category, String description, String author)
        {
            _filmService.AddFilm(title, category, description, author);
        }
        /** Search Film .*/
        public ObservableCollection<Film> SearchFilms(String title, Category category)
        {
            return _filmService.SearchFilms(title, category);
        }
        /** Get Film. */
        public Film GetFilmById(int id)
        {
            return _filmService.GetFilmById(id);
        }
        /** Delete film. */
        public void DeleteFilm(Film film)
        {
            _filmService.DeleteFilm(film);
        }
        /** Get Average. */
        public double GetAverage(int id)
        {
            return _filmService.GetAverage(id);
        }
        /** Get All advices. */
        public ObservableCollection<Advice> GetAllAdvicesById(int id)
        {
            return _filmService.GetAllAdvicesByFilmId(id);
        }
        /** Add Advice. */
        public void AddComment(Film film, String myAdvice, int myScore, User myUser)
        {
            _filmService.AddComment(film, myAdvice, myScore, myUser);
        }
        /** Update User Info. */
        public void UpdateUserInfo(User user)
        {
            _userService.UpdateUserInfo(user);
        }
        /** Update film Info. */
        public void UpdateFilm(Film film)
        {
            _filmService.UpdateFilm(film);
        }
    }
}
