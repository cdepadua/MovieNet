﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieNET_BUS.DAO
{
    class AdviceDao
    {
        private static AdviceDao _adviceDaoInstance;
        private ModelContainer _modelContainer;

        private AdviceDao()
        {
            _modelContainer = new ModelContainer();
        }

        public static AdviceDao AdviceDaoInstance
        {
            get
            {
                if (_adviceDaoInstance == null)
                {
                    _adviceDaoInstance = new AdviceDao();
                }
                return _adviceDaoInstance;
            }
        }

        public void AddAdvice(Film film, String myAdvice, int myScore, User myUser)
        {
            Advice advice = new Advice
            {
                advice = myAdvice,
                score = myScore,
                Film = _modelContainer.FilmSet.Where(f=> f.Id.Equals(film.Id)).SingleOrDefault(),
                User = _modelContainer.UserSet.Where(u=> u.Id.Equals(myUser.Id)).SingleOrDefault()
            };
            _modelContainer.AdviceSet.Add(advice);
            _modelContainer.SaveChanges();
        }
        
        public List<Advice> GetAllAdviceByFilmId(int id)
        {
            return _modelContainer.AdviceSet.Where(a => a.Film.Id.Equals(id)).ToList();
        }
    }
}
