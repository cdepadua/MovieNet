﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieNET_BUS.DAO
{
    class CategoryDao
    {
        private static CategoryDao _categoryDaoInstance;
        private ModelContainer _modelContainer;

        private CategoryDao()
        {
            _modelContainer = new ModelContainer();
        }

        public static CategoryDao CategoryDaoInstance
        {
            get
            {
                if(_categoryDaoInstance == null)
                {
                    _categoryDaoInstance = new CategoryDao();
                }
                return _categoryDaoInstance;
            }
        }

        public List<Category> GetCategoryList()
        {
            return _modelContainer.CategorySet.ToList();
        }

        public Category GetCategoryById(int id)
        {
            return _modelContainer.CategorySet.Where(c => c.Id.Equals(id)).Single();
        }
    }
}
