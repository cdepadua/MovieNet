﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieNET_BUS.DAO
{
    class FilmDao
    {
        private static FilmDao _filmDaoInstance;
        private ModelContainer _modelContainer;

        private FilmDao()
        {
            _modelContainer = new ModelContainer();
        }

        public static FilmDao FilmDaoInstance
        {
            get
            {
                if (_filmDaoInstance == null)
                {
                    _filmDaoInstance = new FilmDao();
                }
                return _filmDaoInstance;
            }
        }

        public void AddFilm(Film film)
        {
            _modelContainer.FilmSet.Add(film);
            _modelContainer.SaveChanges();
        }

        public Film getFilmById(int id)
        {
            return _modelContainer.FilmSet.Where(f => f.Id.Equals(id)).SingleOrDefault();
        }

        public void DeleteFilm(Film film)
        {
            _modelContainer.FilmSet.Remove(film);
            _modelContainer.SaveChanges();
        }

        public void UpdateFilm(Film film)
        {
            _modelContainer.SaveChanges();
        }

        public List<Film> SearchFilm(String title, Category category)
        {
            List<Film> results = null;
            IQueryable<Film> query = _modelContainer.FilmSet;

     
            if (title != "")
            {
                query = query.Where(f => f.title.Contains(title)); 
            }
            if(category != null)
            {
                if (category.Id != 1)
                    query = query.Where(f => f.Category.Id.Equals(category.Id));
            }

            results = query.ToList();
            
            return results;
        }
    }
}
