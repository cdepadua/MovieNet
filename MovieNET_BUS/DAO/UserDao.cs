﻿using MovieNET_BUS.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieNET_BUS.DAO
{
    internal class UserDao
    {

        private static UserDao _userDaoInstance;
        private ModelContainer _modelContainer;

        private UserDao()
        {
            _modelContainer = new ModelContainer();
        }
        public static UserDao UserDaoInstance
        {
            get
            {
                if (_userDaoInstance == null)
                {
                    _userDaoInstance = new UserDao();
                }
                return _userDaoInstance;
            }
        }
        public void createUser(User user)
        {
            _modelContainer.UserSet.Add(user);
            _modelContainer.SaveChanges();
        }
        public User getUserByNickNameAndEmail(User user)
        {
            User userToReturn = null;
            List<User> users = _modelContainer.UserSet.Where(u => u.login.Equals(user.login) || u.email.Equals(user.email)).ToList();
            if (users.Any())
                userToReturn = users[0];
            return userToReturn;
        }
        public User getUserByEmailAndPassword(String email, String password)
        {
            User userToReturn = null;
            User userToTest = _modelContainer.UserSet
                .Where(u => u.email.Equals(email)).SingleOrDefault();
            if (userToTest != null)
            {
                if (StringCipher.Decrypt(userToTest.password, Constants.PASSPHRASE)
                .Equals(StringCipher.Decrypt(password, Constants.PASSPHRASE)))
                {
                    userToReturn = userToTest;
                }
            }
            return userToReturn;
        }

        public void UpdateUserInfo(User _user)
        {
            _modelContainer.SaveChanges();   
        }
    }
}
