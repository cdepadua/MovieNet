
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 09/23/2017 18:48:19
-- Generated from EDMX file: C:\Users\cesar\Desktop\DotNet\MovieNET\MovieNET_BUS\Model.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Database];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_AdviceUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AdviceSet] DROP CONSTRAINT [FK_AdviceUser];
GO
IF OBJECT_ID(N'[dbo].[FK_FilmAdvice]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AdviceSet] DROP CONSTRAINT [FK_FilmAdvice];
GO
IF OBJECT_ID(N'[dbo].[FK_FilmCategory]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[FilmSet] DROP CONSTRAINT [FK_FilmCategory];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[AdviceSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AdviceSet];
GO
IF OBJECT_ID(N'[dbo].[CategorySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CategorySet];
GO
IF OBJECT_ID(N'[dbo].[FilmSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FilmSet];
GO
IF OBJECT_ID(N'[dbo].[UserSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'UserSet'
CREATE TABLE [dbo].[UserSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [login] nvarchar(max)  NOT NULL,
    [password] nvarchar(max)  NOT NULL,
    [lastname] nvarchar(max)  NULL,
    [firstname] nvarchar(max)  NULL,
    [birthday] datetime  NULL,
    [email] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'FilmSet'
CREATE TABLE [dbo].[FilmSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [title] nvarchar(max)  NOT NULL,
    [abstract] nvarchar(max)  NOT NULL,
    [image] nvarchar(max)  NULL,
    [author] nvarchar(max)  NULL,
    [Category_Id] int  NOT NULL
);
GO

-- Creating table 'AdviceSet'
CREATE TABLE [dbo].[AdviceSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [score] float  NOT NULL,
    [advice] nvarchar(max)  NOT NULL,
    [Film_Id] int  NOT NULL,
    [User_Id] int  NOT NULL
);
GO

-- Creating table 'CategorySet'
CREATE TABLE [dbo].[CategorySet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [name] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'UserSet'
ALTER TABLE [dbo].[UserSet]
ADD CONSTRAINT [PK_UserSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'FilmSet'
ALTER TABLE [dbo].[FilmSet]
ADD CONSTRAINT [PK_FilmSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AdviceSet'
ALTER TABLE [dbo].[AdviceSet]
ADD CONSTRAINT [PK_AdviceSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CategorySet'
ALTER TABLE [dbo].[CategorySet]
ADD CONSTRAINT [PK_CategorySet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Film_Id] in table 'AdviceSet'
ALTER TABLE [dbo].[AdviceSet]
ADD CONSTRAINT [FK_FilmAdvice]
    FOREIGN KEY ([Film_Id])
    REFERENCES [dbo].[FilmSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FilmAdvice'
CREATE INDEX [IX_FK_FilmAdvice]
ON [dbo].[AdviceSet]
    ([Film_Id]);
GO

-- Creating foreign key on [User_Id] in table 'AdviceSet'
ALTER TABLE [dbo].[AdviceSet]
ADD CONSTRAINT [FK_AdviceUser]
    FOREIGN KEY ([User_Id])
    REFERENCES [dbo].[UserSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AdviceUser'
CREATE INDEX [IX_FK_AdviceUser]
ON [dbo].[AdviceSet]
    ([User_Id]);
GO

-- Creating foreign key on [Category_Id] in table 'FilmSet'
ALTER TABLE [dbo].[FilmSet]
ADD CONSTRAINT [FK_FilmCategory]
    FOREIGN KEY ([Category_Id])
    REFERENCES [dbo].[CategorySet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FilmCategory'
CREATE INDEX [IX_FK_FilmCategory]
ON [dbo].[FilmSet]
    ([Category_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------