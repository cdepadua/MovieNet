﻿using MovieNET_BUS.DAO;
using MovieNET_BUS.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieNET_BUS.Services
{
    internal class UserService
    {
        private static UserService _userServiceInstance;
        private UserDao _userDao;

        private UserService()
        {
            _userDao = UserDao.UserDaoInstance;
        }
        public static UserService UserServiceInstance
        {
            get
            {
                if(_userServiceInstance == null)
                {
                    _userServiceInstance = new UserService();
                }
                return _userServiceInstance;
            }
        }
        public void userRegistration(User user)
        {
            if (_userDao.getUserByNickNameAndEmail(user) == null)
            {
                _userDao.createUser(user);
            }
            else
            {
                throw new MovieNETException("Un utilisateur avec le même nom ou la même adresse email existe déjà");
            }
        }
        public User userLogin(String email, String password)
        {
            User userToReturn = _userDao.getUserByEmailAndPassword(email, password);
            if (userToReturn == null)
                throw new MovieNETException("Le mail ou le mot de passe sont incorrects");
            else
                return userToReturn;
        }

        public void UpdateUserInfo(User user)
        {
            _userDao.UpdateUserInfo(user);
        }
    }
}
