﻿using MovieNET_BUS.DAO;
using MovieNET_BUS.Exceptions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieNET_BUS.Services
{
    class FilmService
    {
        private static FilmService _filmServiceInstance;
        private FilmDao _filmDaoInstance;
        private CategoryDao _categoryDaoInstance;
        private AdviceDao _adviceDaoInstance;

        private FilmService()
        {
            _filmDaoInstance = FilmDao.FilmDaoInstance;
            _categoryDaoInstance = CategoryDao.CategoryDaoInstance;
            _adviceDaoInstance = AdviceDao.AdviceDaoInstance;

        }

        public static FilmService FilmServiceInstance
        {
            get
            {
                if (_filmServiceInstance == null)
                {
                    _filmServiceInstance = new FilmService();
                }
                return _filmServiceInstance;
            }
        }

        public ObservableCollection<Category> GetCategories()
        {
            ObservableCollection<Category> oc = new ObservableCollection<Category>();
            List<Category> list = _categoryDaoInstance.GetCategoryList();
            foreach (var item in list)
                oc.Add(item);
            return oc;
        }

        public void AddFilm(String title, Category category, String description, String author)
        {
            if (title == "" || category == null || description == "")
            {
                throw new MovieNETException("Les champs ne sont pas tous remplis");
            }
            else
            {
                Film film = new Film
                {
                    title = title,
                    author = author,
                    @abstract = description,
                    Category = category
                };
         
                _filmDaoInstance.AddFilm(film);
            }
            
        }

        public void DeleteFilm(Film film)
        {
            _filmDaoInstance.DeleteFilm(film);
        }

        public ObservableCollection<Film> SearchFilms(String title, Category category)
        {
            ObservableCollection<Film> oc = new ObservableCollection<Film>();

            List<Film> list = _filmDaoInstance.SearchFilm(title, category);
            if (list != null)
            {
                foreach (var item in list)
                    oc.Add(item);
            }
            return oc;
        }

        public Film GetFilmById(int id)
        {
            return _filmDaoInstance.getFilmById(id);
        }

        public double GetAverage(int id)
        {
            double average = 0;
            int nbAdvices = 0;
            List<Advice> advices = _adviceDaoInstance.GetAllAdviceByFilmId(id);
            advices.ForEach(a =>
            {
                average += a.score;
                nbAdvices++;
            });
            return Math.Round(average / nbAdvices, 1);
        }

        public ObservableCollection<Advice> GetAllAdvicesByFilmId(int id)
        {
            ObservableCollection<Advice> oc = new ObservableCollection<Advice>();

            List<Advice> advices = _adviceDaoInstance.GetAllAdviceByFilmId(id);

            advices.ForEach(a =>
            {
                oc.Add(a);
            });

            return oc;
        }

        public void AddComment(Film film, String myAdvice, int myScore, User myUser)
        {
            if (myAdvice == "" || myAdvice == "Ajouter un commentaire" || myScore == 0 )
            {
                throw new MovieNETException("Veuiller remplir tous les champs du commentaire");
            }
            else
            {
                _adviceDaoInstance.AddAdvice(film, myAdvice, myScore, myUser);
            }
        }

        public void UpdateFilm(Film film)
        {
            if (film.title == "" || film.@abstract =="" || film.Category == null)
            {
                throw new MovieNETException("Les champs ne sont pas tous remplis");
            }
            else
            {
                _filmDaoInstance.UpdateFilm(film);
            }
        }
    }
}
