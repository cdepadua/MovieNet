Exemple des modifications a apporter au fichier ViewModelLocator pour les applications WPF

namespace MonApplication.ViewModel
{
    using GalaSoft.MvvmLight.Ioc;
    using Microsoft.Practices.ServiceLocation;
    using Navegar;

    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            //1. Enregistrer la classe de navigation dans l'IOC
            if (!SimpleIoc.Default.IsRegistered<INavigation>())
            {
                SimpleIoc.Default.Register<INavigation, Navigation>();    
            }

            //2. Generer le viewmodel principal, le type du ViewModel peut etre n'importe lequel
            //Cette generation va permettre de creer, au sein de la 
            //navigation, une instance unique pour le viewmodel principal,
            //qui sera utilisee par la classe de navigation
            SimpleIoc.Default.GetInstance<INavigation>()
                             .GenerateMainViewModelInstance<MainViewModel>();
        }

        public MainViewModel Main
        {
            //3. Retrouve le viewmodel principal
            get
            {
                return SimpleIoc.Default.GetInstance<INavigation>()
                                .GetMainViewModelInstance<MainViewModel>();
            }
        }
    }
}