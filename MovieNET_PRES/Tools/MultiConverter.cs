﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Markup;
using MovieNET_PRES;

namespace MovieNET_PRES.Tools
{
    [MarkupExtensionReturnType(typeof(PlainMultiValueConverter))]
    //[ValueConversion(typeof(object[]), typeof(object))]
    public class PlainMultiValueConverter : MarkupExtension,
        IMultiValueConverter
    {
        public static PlainMultiValueConverter converter = null;

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (null == converter)
            {
                converter = new PlainMultiValueConverter();
            }
            return converter;
        }

        public object Convert(object[] values, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            return values.ToList();
        }

        public object[] ConvertBack(object value, Type[] targetTypes,
            object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
