﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using Navegar.Libs.Class;
using Navegar.Libs.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MovieNET_PRES;
using System.Windows.Threading;

namespace MovieNET_PRES.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private readonly DispatcherTimer _timerLoadAccueil;
        private ViewModelBase _currentView;
        public MainViewModel()
        {
            _timerLoadAccueil = new DispatcherTimer
            {
                Interval = new TimeSpan(0, 0, 0, 0, 500)
            };
            _timerLoadAccueil.Tick += LoadAccueil;
            _timerLoadAccueil.Start();
        }
        [CurrentViewNavigation]
        public ViewModelBase CurrentView
        {
            get { return _currentView; }
            set
            {
                _currentView = value;
                RaisePropertyChanged("CurrentView");
            }
        }

        //Navigate to the first view
        private void LoadAccueil(object sender, EventArgs e)
        {
            _timerLoadAccueil.Stop();
            ServiceLocator.Current.GetInstance<INavigationWpf>().NavigateTo<SignupViewModel>();
        }
    }
}
