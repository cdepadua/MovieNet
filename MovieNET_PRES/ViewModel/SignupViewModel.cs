using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Windows.Controls;
using MovieNET_BUS;
using MovieNET_PRES;
using Navegar.Libs.Interfaces;
using Microsoft.Practices.ServiceLocation;
using MovieNET_BUS.Exceptions;
using MovieNET_PRES.ViewModel;

namespace MovieNET_PRES.ViewModel
{

    public class SignupViewModel : BlankViewModelBase
    {
        /** Constructor. */
        public SignupViewModel()
        {
            _message = "";
            _email = "";
            _nickName = "";
            _movieNetBusiness = MovieNETBusiness.MovieNETBusinessInstance;
            RegisterUserCommand = new RelayCommand<object>(RegisterUser);
            ToLoginCommand = new RelayCommand(ToLogin);
        }

        /** email. */
        private string _email;
        /** nickname. */
        private string _nickName;
        /** message d'erreur. */
        private string _message;

        /** Facade. */
        private MovieNETBusiness _movieNetBusiness;

        /** Getters and Setters */
        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                RaisePropertyChanged();
            }
        }
        public string NickName
        {
            get { return _nickName; }
            set
            {
                _nickName = value;
                RaisePropertyChanged();
            }
        }
        public string Message
        {
            get { return _message; }
            set
            {
                _message = value;
                RaisePropertyChanged();
            }
        }
        /** Commands. */
        public RelayCommand<object> RegisterUserCommand { get; }
        public RelayCommand ToLoginCommand { get; }

        void RegisterUser(object parameter)
        {
            var pswBoxes = parameter as List<object>;
            PasswordBox password = pswBoxes[0] as PasswordBox;
            PasswordBox passwordConfirm = pswBoxes[1] as PasswordBox;
            String psw = _movieNetBusiness.Encrypt(password.Password);
            String pswConf = _movieNetBusiness.Encrypt(passwordConfirm.Password);

            if (VerifyForm(password.Password,passwordConfirm.Password))
            {
                User user = new User();
                user.email = Email;
                user.login = NickName;
                user.password = psw;
                try
                {
                    _movieNetBusiness.userRegistration(user);
                    User userLogin = _movieNetBusiness.userLogin(Email, user.password);
                    ServiceLocator.Current.GetInstance<INavigationWpf>().NavigateTo<SearchViewModel>(userLogin);

                }
                catch(MovieNETException e)
                {
                    Message = e.Message;
                }
            }

        }

        void ToLogin()
        {
            ServiceLocator.Current.GetInstance<INavigationWpf>().NavigateTo<LoginViewModel>();
        }

        /** Methodes. */
        private Boolean VerifyForm(String psw, String pswConfirm)
        {
            Boolean formIsValid = true;
            Message = "";
            if (psw.Length <= 0 || pswConfirm.Length <= 0)
            {
                Message = "Le mot de passe est obligatoire";
                formIsValid = false;
            }
            else if ( psw != pswConfirm)
            {
                Message = "Les mots de passe ne sont pas identiques";
                formIsValid = false;
            }
            else if (NickName.Length <= 0)
            {
                Message = "Le pseudo est obligatoire";
                formIsValid = false;
            }
            try
            {
                var addr = new System.Net.Mail.MailAddress(Email);
                return addr.Address == Email;
            }
            catch
            {
                Message = "Le mail n'est pas valide";
                formIsValid = false;
            }
            return formIsValid;
        }
    }

}