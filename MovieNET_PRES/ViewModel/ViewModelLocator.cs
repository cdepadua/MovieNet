/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:MovieNET_PRES"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using Navegar.Libs.Interfaces;
using MovieNET_PRES;
using Navegar.Plateformes.Net.WPF;

namespace MovieNET_PRES.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {

            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            //1. Enregistrer la classe de navigation dans l'IOC
            SimpleIoc.Default.Register<INavigationWpf, Navigation>();


            SimpleIoc.Default.GetInstance<INavigationWpf>().GenerateMainViewModelInstance<MainViewModel>();
        }

        public MainViewModel Main
        {
            //3. Retrouve le viewmodel principal
            get
            {
                return SimpleIoc.Default.GetInstance<INavigationWpf>()
                                .GetMainViewModelInstance<MainViewModel>();
            }
        }
        public SignupViewModel Signup
        {
            //3. Retrouve le viewmodel principal
            get
            {
                return SimpleIoc.Default.GetInstance<INavigationWpf>()
                                .GetMainViewModelInstance<SignupViewModel>();
            }
        }
    }
}