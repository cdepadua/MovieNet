﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.ServiceLocation;
using MovieNET_BUS;
using Navegar.Libs.Interfaces;
using System;
using System.Collections.ObjectModel;
namespace MovieNET_PRES.ViewModel
{
    class SearchViewModel : BlankViewModelBase
    {
        private ViewModelBase _menu;

        private User _user;

        private ObservableCollection<Category> _categories;

        private String _message;

        private Category _selectedCategory;

        private String _title;

        private ObservableCollection<Film> _films;

        

        /** facade. */
        private MovieNETBusiness _movieNetBusiness = MovieNETBusiness.MovieNETBusinessInstance;

        public ObservableCollection<Film> Films
        {
            get { return _films; }
            set
            {
                _films = value;
                RaisePropertyChanged();
            }
        }

        public String Message
        {
            get { return _message; }
            set
            {
                _message = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<Category> Categories
        {
            get { return _categories; }
            set
            {
                _categories = value;
                RaisePropertyChanged();
            }
        }

        public User CurrentUser
        {
            get { return _user; }
        }

        public Category SelectedCategory
        {
            get { return _selectedCategory; }
            set
            {
                _selectedCategory = value;
                RaisePropertyChanged();
            }
        }

        public String Title
        {
            get { return _title; }
            set
            {
                _title = value;
                RaisePropertyChanged();
            }
        }

        /** Command. */
        public RelayCommand SearchCommand { get; }
        public RelayCommand<int> GoToFilmCommand { get; }
        public void Search()
        {
            Films = _movieNetBusiness.SearchFilms(Title, SelectedCategory);

        }
        public void GoToFIlm(int id)
        {
           Film film =  _movieNetBusiness.GetFilmById(id);
           ServiceLocator.Current.GetInstance<INavigationWpf>().NavigateTo<FilmViewModel>(true, new object[] { _user, film });
        }
        public ViewModelBase Menu
        {
            get { return _menu; }
        }
  
        /** Construcor. */
        public SearchViewModel(User user)
        {
            Title = "";
            Categories = _movieNetBusiness.GetCategoryList();
            SelectedCategory = null;
            _user = user;
            _menu = MenuViewModel.getMenuViewModel(_user);

            SearchCommand = new RelayCommand(Search);
            GoToFilmCommand = new RelayCommand<int>(GoToFIlm);
            Search();
        }
        public void SetMsg(String msg)
        {
            Message = msg;
            Search();
        }
    }
}
