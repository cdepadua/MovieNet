﻿using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.ServiceLocation;
using MovieNET_BUS;
using MovieNET_BUS.Exceptions;
using Navegar.Libs.Interfaces;
using System.Windows.Controls;

namespace MovieNET_PRES.ViewModel
{
    class LoginViewModel : BlankViewModelBase
    {

        /** email. */
        private string _email;
        /** message. */
        private string _message;
        /** facade. */
        private MovieNETBusiness _movieNetBusiness;

        /** Constructor. */
        public LoginViewModel()
        {
            _email = "";
            _message = "";
            _movieNetBusiness = MovieNETBusiness.MovieNETBusinessInstance;
            ToRegistrationCommand = new RelayCommand(ToRegistration);
            LoginCommand = new RelayCommand<object>(Login);
        }
        /** Getters an Setters. */
        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                RaisePropertyChanged();
            }
        }
        public string Message
        {
            get { return _message; }
            set
            {
                _message = value;
                RaisePropertyChanged();
            }
        }

        /** Commands. */
        public RelayCommand ToRegistrationCommand { get; }
        public RelayCommand<object> LoginCommand { get; }
        public void ToRegistration()
        {
            ServiceLocator.Current.GetInstance<INavigationWpf>().NavigateTo<SignupViewModel>();
        }
        public void Login(object param)
        {
            Message = "";
            PasswordBox passwordBox = param as PasswordBox;
            string password = _movieNetBusiness.Encrypt(passwordBox.Password);
            try
            {
                User user = _movieNetBusiness.userLogin(Email, password);
                ServiceLocator.Current.GetInstance<INavigationWpf>().NavigateTo<SearchViewModel>(user);
            }
            catch(MovieNETException e)
            {
                Message = e.Message;
            }
        }
    }
}
