﻿using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.ServiceLocation;
using MovieNET_BUS;
using Navegar.Libs.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MovieNET_PRES;
using System.Threading.Tasks;

namespace MovieNET_PRES.ViewModel
{
    class MenuViewModel : BlankViewModelBase
    {
        private static MenuViewModel _instance;
        private User _user;
        private MenuViewModel(User user)
        {
            _user = user;
            ToAddFilmCommand = new RelayCommand(ToAddFilm);
            ToHomeCommand = new RelayCommand(ToHome);
            ToProfileCommand = new RelayCommand(ToProfile);
        }

        public User CurrentUser
        {
            get { return _user; }
        }
        public static MenuViewModel getMenuViewModel(User user)
        {
            if( _instance == null)
            {
                _instance = new MenuViewModel(user);
            }
            return _instance;
        }

        /** Commands. */
        public RelayCommand ToAddFilmCommand { get; }
        public void ToAddFilm()
        {
            ServiceLocator.Current.GetInstance<INavigationWpf>().NavigateTo<AddFilmViewModel>(true,new object[] { _user, null });
        }

        public RelayCommand ToHomeCommand { get; }
        public void ToHome()
        {
            ServiceLocator.Current.GetInstance<INavigationWpf>().NavigateTo<SearchViewModel>(_user);
        }

        public RelayCommand ToProfileCommand { get; }
        public void ToProfile()
        {
            ServiceLocator.Current.GetInstance<INavigationWpf>().NavigateTo<ModifieAccountViewModel>(_user);
        }
    }
}
