﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MovieNET_BUS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MovieNET_PRES.ViewModel
{
    class ModifieAccountViewModel : BlankViewModelBase
    {
        private User _user;

        private ViewModelBase _menu;

        private String _birthday;

        private String _message;

        private String _error;

        /** Constructor. */
        public ModifieAccountViewModel(User user)
        {
            UserInstance = user;
            UpdateUserInfoCommand = new RelayCommand<object>(UpdateUserInfo);
            _menu = MenuViewModel.getMenuViewModel(_user);
        }

        /** facade. */
        private MovieNETBusiness _movieNetBusiness = MovieNETBusiness.MovieNETBusinessInstance;

        /** Properties. */
        public String Message
        {
            get { return _message; }
            set
            {
                _message = value;
                RaisePropertyChanged();
            }
        }
        public String Error
        {
            get { return _error; }
            set
            {
                _error = value;
                RaisePropertyChanged();
            }
        }
        public User UserInstance
        {
            get { return _user; }
            set
            {
                _user = value;
                RaisePropertyChanged();
            }
        }
        public ViewModelBase Menu
        {
            get { return _menu; }
        }

        public String Birthday
        {
            get { return _birthday; }
            set
            {
                _birthday = value;
                RaisePropertyChanged();
            }
        }

        /** Commands. */
        public RelayCommand<object> UpdateUserInfoCommand { get; }
        public void UpdateUserInfo(object param)
        {
            var pswBoxes = param as List<object>;
            PasswordBox password = pswBoxes[0] as PasswordBox;
            PasswordBox passwordConfirm = pswBoxes[1] as PasswordBox;
            String psw = _movieNetBusiness.Encrypt(password.Password);
            String pswConf = _movieNetBusiness.Encrypt(passwordConfirm.Password);

            if (password.Password != passwordConfirm.Password)
            {
                Message = "";
                Error = "Les mots de passe ne sont pas identiques";
                return;
            }
            else
            {
                if(password.Password != "")
                {
                    UserInstance.password = psw;
                }
            }

            DateTime temp;
            if (DateTime.TryParse(Birthday, out temp))
            {
                UserInstance.birthday = temp;
            }
            else
            {
                Error = "Le format date n'est pas correct";
                return;
            }

                if (UserInstance.email == "")
            {
                Message = "";
                Error = "Le mail est obligatoire";
                return;
            }
            if(UserInstance.login == "")
            {
                Message = "";
                Error = "Le login est obligatoire";
                return;
            }

            _movieNetBusiness.UpdateUserInfo(UserInstance);
            Error = "";
            Message = "Votre profil a bien été modifié";
        }
    }
}
