﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.ServiceLocation;
using MovieNET_BUS;
using MovieNET_BUS.Exceptions;
using Navegar.Libs.Interfaces;
using System;
using System.Collections.ObjectModel;

namespace MovieNET_PRES.ViewModel
{
    class FilmViewModel : BlankViewModelBase
    {
        private String _message;

        private String _error;

        private ViewModelBase _menu;

        private User _user;

        private Film _film;

        private double _average;

        private int _selectedScore;

        private String _newComment;

        private ObservableCollection<Advice> _advices;

        /** Facade. */
        private MovieNETBusiness _movieNetBusiness;

        /** constructor. */
        public FilmViewModel(User user, Film film)
        {
            _user = user;
            SelectedFilm = film;
            _menu = MenuViewModel.getMenuViewModel(_user);
            DeleteFilmCommand = new RelayCommand(DeleteFilm);
            ToAddFilmCommand = new RelayCommand(ToAddFilm);
            PublishCommentCommand = new RelayCommand(PublishComment);
            _movieNetBusiness = MovieNETBusiness.MovieNETBusinessInstance;
            _average = _movieNetBusiness.GetAverage(SelectedFilm.Id);
            NewComment = "Ajouter un commentaire";
            SelectedScore = 0;
            GetAdvices();
        }

        /** properties. */

        public String Message
        {
            get { return _message; }
            set
            {
                _message = value;
                RaisePropertyChanged();
            }
        }

        public String Error
        {
            get { return _error; }
            set
            {
                _error = value;
                RaisePropertyChanged();
            }
        }

        public Film SelectedFilm
        {
            get { return _film; }
            set
            {
                _film = value;
                RaisePropertyChanged();
            }
        }

        public double Average
        {
            get { return _average; }
            set
            {
                _average = value;
                RaisePropertyChanged();
            }
        }

        public ViewModelBase Menu
        {
            get { return _menu; }
        }

        public ObservableCollection<Advice> Advices
        {
            get { return _advices; }
            set
            {
                _advices = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<int> Scores { get; set; } = new ObservableCollection<int> { 1, 2, 3, 4, 5 };

        public String NewComment
        {
            get { return _newComment; }
            set
            {
                _newComment = value;
                RaisePropertyChanged();
            }
        }

        public int SelectedScore
        {
            get { return _selectedScore; }
            set
            {
                _selectedScore = value;
                RaisePropertyChanged();
            }
        }

        /** Commands. */
        public RelayCommand DeleteFilmCommand { get; }
        public void DeleteFilm()
        {
            _movieNetBusiness.DeleteFilm(SelectedFilm);
            ServiceLocator.Current.GetInstance<INavigationWpf>().NavigateTo<SearchViewModel>(this, new object[] { _user }, "SetMsg", new object[] { "Le film a été supprimé avec succes" }, false);
        }

        public RelayCommand PublishCommentCommand { get; }
        public void PublishComment()
        {
            try
            {
                _movieNetBusiness.AddComment(_film, NewComment, SelectedScore, _user);
                Average = _movieNetBusiness.GetAverage(SelectedFilm.Id);
                GetAdvices();
                SelectedScore = 0;
                NewComment = "Ajouter un commentaire";
                Message = "Votre avis a bien été enregistré";
                Error = "";
            }
            catch(MovieNETException e)
            {
                Error = e.Message;
                Message = "";
            }
            
        }

        /** Commands. */
        public RelayCommand ToAddFilmCommand { get; }
        public void ToAddFilm()
        {
            ServiceLocator.Current.GetInstance<INavigationWpf>().NavigateTo<AddFilmViewModel>(true, new object[] { _user, _film });
        }

        /** Method. */
        public void GetAdvices()
        {
            Advices = _movieNetBusiness.GetAllAdvicesById(SelectedFilm.Id);
        }
    }
}
