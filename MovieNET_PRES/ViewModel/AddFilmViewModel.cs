﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.ServiceLocation;
using MovieNET_BUS;
using MovieNET_BUS.Exceptions;
using Navegar.Libs.Interfaces;
using System;
using System.Collections.ObjectModel;

namespace MovieNET_PRES.ViewModel
{
    class AddFilmViewModel : BlankViewModelBase
    {
        private Film _filmInstance;

        private String _message;

        private String _error;

        private String _title;

        private String _author;

        private String _description;

        private ViewModelBase _menu;

        private User _user;

        public String Message
        {
            get { return _message; }
            set
            {
                _message = value;
                RaisePropertyChanged();
            }
        }

        public String Title
        {
            get { return _title; }
            set
            {
                _title = value;
                RaisePropertyChanged();
            }
        }
     

        public String Description
        {
            get { return _description; }
            set
            {
                _description = value;
                RaisePropertyChanged();
            }
        }

        public Category SelectedCategory { set; get; }

        public ViewModelBase Menu
        {
            get { return _menu; }
        }

        public Film FilmInstance
        {
            get { return _filmInstance; }
            set
            {
                _filmInstance = value;
                RaisePropertyChanged();
            }
        }

        public String Author
        {
            get { return _author; }
            set
            {
                _author = value;
                RaisePropertyChanged();
            }
        }
        public String Button { get; set; }

        public String Error
        {
            get { return _error; }
            set
            {
                _error = value;
                RaisePropertyChanged();
            }
        }

        /** facade. */
        private MovieNETBusiness _movieNetBusiness;

        /** Commands. */
        public RelayCommand AddFilmCommand { get; }
        public void AddFilm()
        {
            try
            {
                _movieNetBusiness.AddFilm(_title, SelectedCategory, _description, _author);
                ServiceLocator.Current.GetInstance<INavigationWpf>().NavigateTo<SearchViewModel>(this, new object[] { _user }, "SetMsg", new object[] { "Le film a été rajouté avec succes" }, false);
            }
            catch (MovieNETException e)
            {
                Error = e.Message;
            }
            
        }
        public void UpdateFilm()
        {
            try
            {
                _filmInstance.title = _title;
                _filmInstance.Category = SelectedCategory;
                _filmInstance.author = _author;
                _filmInstance.@abstract = _description;
                _movieNetBusiness.UpdateFilm(_filmInstance);
                ServiceLocator.Current.GetInstance<INavigationWpf>().NavigateTo<SearchViewModel>(this, new object[] { _user }, "SetMsg", new object[] { "Le film a été modifié avec succes" }, false);
            }
            catch (MovieNETException e)
            {
                Message = e.Message;
            }
        }

        public ObservableCollection<Category> Categories {get;set;}

        public AddFilmViewModel(User user, Film filmInstance)
        {
            _user = user;
            _filmInstance = filmInstance;
            if(_filmInstance != null)
            {
                _title = filmInstance.title;
                _author = filmInstance.author;
                _description = filmInstance.@abstract;
                SelectedCategory = filmInstance.Category;
                AddFilmCommand = new RelayCommand(UpdateFilm);
                Button = "Modifier la fiche";
            }
            else
            {
                _title = "";
                _author = "";
                _description = "";
                SelectedCategory = null;
                AddFilmCommand = new RelayCommand(AddFilm);
                Button = "Ajouter un film";
            }
            
            _menu = MenuViewModel.getMenuViewModel(_user);
            _movieNetBusiness = MovieNETBusiness.MovieNETBusinessInstance;
            Categories = _movieNetBusiness.GetCategoryList(); 
        }
    }
}
